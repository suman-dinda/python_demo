import webbrowser
import speech_recognition as sr
import subprocess
r = sr.Recognizer()
print("Welcome to my App")
print("Enter your requirements as voice :" , end='')
with sr.Microphone() as source:
    print("Listening...")
    audio = r.listen(source)
    print("speech recorded !! Executing")

ch = r.recognize_google(audio)

if (("run" in ch) or ("execute" in ch)) and ("date" in ch):
    webbrowser.open("http://34.67.253.207/cgi-bin/formapi?x=date")
elif (("run" in ch) or ("execute" in ch)) and ("calendar" in ch):
    webbrowser.open("http://34.67.253.207/cgi-bin/formapi?x=cal")
else:
    print("not understand")
